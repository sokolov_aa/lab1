# С помощью данного пакета можно развернуть Apache Airflow с подключением Spark и созданием пайплайна.
## Содержит DAG:
## - productivity: расчёт быстродействия циклов for и while.

Команды сборки:
1. docker-compose up -d # собираем образы и контейнеры локально, либо раннером. Деплой раннера по скрипту и инструкции из репозитория https://gitlab.com/sokolov_aa/lab3.git
2. docker ps # смотрим реестр контейнеров и их статус
3. Зайти на Airflow через браузер локально по адресу http://localhost:8080/
4. --username admin --password admin
5. Admin/Connections: Add Connection
- Connection id: spark_local
- Connection Type: Spark
- Host: spark://spark-master
- Port: 7077
6. В админке Spark(http://localhost:4040) видно воркер и выполненную таску (таски).
7. Zabbix http://localhost:8082 , креды Admin | zabbix
8. Grafana http://localhost:3000/
---
![Screenshot_1](screenshots/Screenshot_1.jpg)
---
![Screenshot_2](screenshots/Screenshot_2.jpg)
---
![Screenshot_3](screenshots/Screenshot_3.jpg)

![Screenshot_4](screenshots/Screenshot_4.jpg)

![Screenshot_5](screenshots/Screenshot_5.jpg)

![Screenshot_6](screenshots/Screenshot_6.jpg)

![Screenshot_7](screenshots/Screenshot_7.jpg)

![Screenshot_8](screenshots/Screenshot_8.jpg)

![Screenshot_9](screenshots/Screenshot_9.jpg)

