# Измерение скорости цикла for
import datetime

# Import the necessary modules
from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession

from mean_list import ready_list, value

conf = SparkConf().setAppName("My PySpark App").setMaster("spark://spark-master:7077")
sc = SparkContext(conf=conf)

# Start the SparkSession
spark = SparkSession.builder \
                    .config(conf=conf) \
                    .getOrCreate()

# Измерение скорости цикла for
def def_time_for(ready_list, value):
    start = datetime.datetime.now()
    found = False
    for number in ready_list:
        if number == value:
            found = True
    time_for = datetime.datetime.now() - start
    return time_for

measurement_result_for = def_time_for(ready_list, value)

# Выводим результат
print("Скорость цикла for:", measurement_result_for)

# Stop the SparkSession
spark.stop()
