# Список случайных значений
import random

# Import the necessary modules
from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession

conf = SparkConf().setAppName("My PySpark App").setMaster("spark://spark-master:7077")
sc = SparkContext(conf=conf)
# Start the SparkSession
spark = SparkSession.builder \
                    .config(conf=conf) \
                    .getOrCreate()

value = 10000 #Граница диапазона значений
fake_list = []

def generate_random_list(fake_list, value):
    for _ in range(1000000):
        fake_list.append(random.randint(0, value))
    return fake_list

ready_list = generate_random_list(fake_list, value)


# Создаем RDD
rdd = sc.parallelize(ready_list)

# Вычисляем среднее значение чисел
mean_list = rdd.mean()

# Выводим результат
print("Среднее значение списка равно:", mean_list)

# Stop the SparkSession
spark.stop()
