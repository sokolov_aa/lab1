from airflow import DAG
from airflow.contrib.operators.spark_submit_operator import SparkSubmitOperator
from airflow.utils.dates import days_ago

default_args = {
    'owner': 'airflow',
    'start_date': days_ago(0, 0, 0, 0, 0)
}

with DAG(
    'productivity',
    default_args=default_args,
    schedule_interval=None,
    description='Measuring the speed of for and while loops'
) as dag:

    spark_job1 = SparkSubmitOperator(task_id='generate_list_id',
                                    application=f'/opt/airflow/spark/mean_list.py',
                                    name='mean_list_job',
                                    conn_id='spark_local')

    spark_job2 = SparkSubmitOperator(task_id='time_for_id',
                                    application=f'/opt/airflow/spark/time_for.py',
                                    name='time_for_job',
                                    conn_id='spark_local')

    spark_job3 = SparkSubmitOperator(task_id='time_while_id',
                                    application=f'/opt/airflow/spark/time_while.py',
                                    name='time_while_job',
                                    conn_id='spark_local')


    spark_job1 >> spark_job2 >> spark_job3
